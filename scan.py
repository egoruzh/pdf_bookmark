"""
compile command
pyinstaller --onedir --noconfirm --add-data "D:/temp/pdf/pdf_bookmark/poppler/*;./poppler" --add-data "D:/temp/pdf/pdf_bookmark/Tesseract-OCR/*;./Tesseract-OCR" --add-data "D:/temp/pdf/pdf_bookmark/Tesseract-OCR/tessdata/*;./Tesseract-OCR/tessdata" .\scan.py
"""
import json
import os
import re
import shutil
from pathlib import Path

import PyPDF2
import easygui
import pytesseract
from PIL import Image
from natsort import natsorted
from pdf2image import convert_from_path, pdfinfo_from_path

poppler_path = r"./poppler"


def current_path(dir_path):
    if hasattr(os.system, '_MEIPASS'):
        return os.path.join(os.system._MEIPASS, dir_path)
    return os.path.join(".", dir_path)


os.environ["PATH"] += os.pathsep + os.pathsep.join([current_path("poppler")])
os.environ["PATH"] += os.pathsep + os.pathsep.join([current_path("Tesseract-OCR")])
pytesseract.pytesseract.tesseract_cmd = r'./Tesseract-OCR/tesseract.exe'
tessdata_dir_config = '--tessdata-dir "./Tesseract-OCR/tessdata"'
path_to_img = './images'
h1 = ["ПОВІДОМЛЕННЯ", "РАПОРТ", "ПОСТАНОВА", "ПРОТОКОЛ", "ВИТЯГ", "ДОРУЧЕННЯ", "УХВАЛА", "КЛОПОТАННЯ",
      "ВИСНОВОК ЕКСПЕРТА", "ПАМЯТКА"]

path_file = easygui.fileopenbox(msg='Please locate the pdf file',
                                title='Pdf',
                                filetypes='*.pdf')

file_name = Path(path_file).stem
output_pdf = "bookmarks_" + file_name + ".pdf"
json_result_file_name = "result.json"
perPage = 10
result = dict()

try:
    os.mkdir(path_to_img)
    print('folder ' + str(Path(path_to_img)) + ' created')
except:
    print('folder ' + str(Path(path_to_img)) + ' exist')


def pdf_to_img():
    info = pdfinfo_from_path(path_file, userpw=None)
    max_pages = info["Pages"]
    print(str(max_pages) + " pages")
    for page in range(1, max_pages + 1, perPage):
        last_page = min(page + perPage - 1, max_pages)
        first_page = page
        print("convert to JPEG " + str(first_page) + " - " + str(last_page) + " series ...")
        images = convert_from_path(path_file, 500,
                                   first_page=first_page,
                                   last_page=last_page)
        for i in range(len(images)):
            item = str(page + i)
            print("work " + item + ' ...')
            img_name = path_to_img + '/' + item + '.jpg'
            images[i].save(img_name, 'JPEG')
            print("success " + item)


def save_text_to_json():
    imgs = natsorted(os.listdir(path_to_img), key=lambda y: y.lower())
    for idx, imgName in enumerate(imgs):
        print("start " + imgName)
        text_page = pytesseract.image_to_string(Image.open(str(path_to_img + '/' + imgName)), lang='ukr',
                                                config=tessdata_dir_config)
        lines = text_page.split('\n')
        result[idx] = list(filter(None, lines))
        print("end " + imgName)
    with open(json_result_file_name, 'w') as f:
        json.dump(result, f, ensure_ascii=False, indent=4)
    shutil.rmtree(path_to_img, ignore_errors=True)


def add_bookmarks():
    output = PyPDF2.PdfFileWriter()
    input_pdf = PyPDF2.PdfFileReader(open(path_file, 'rb'), strict=False)
    bookmarks = dict()
    bookmarks_parent = dict()
    regex = re.compile('[^a-zA-Zа-яА-ЯІіЇїЄє]')
    with open(json_result_file_name) as json_file:
        json_data = json.load(json_file)
        for page in json_data:
            for search in h1:
                striped_json_data = list()
                strip_h1 = regex.sub('', search)
                for idx, string in enumerate(json_data[page]):
                    striped_json_data.insert(idx, regex.sub('', string))

                if strip_h1 in striped_json_data:
                    str_search = striped_json_data.index(strip_h1)
                    description = []
                    try:
                        for r in range(5):
                            c = 0
                            if len(regex.sub('', json_data[page][str_search + r + 1])) != 0:
                                c = c + 1
                                description.append(json_data[page][str_search + r + 1])
                                if c > 2:
                                    break
                    except IndexError:
                        description = ['Нет описания']
                    bookmarks_parent[search] = search
                    bookmarks[search, page, str_search] = [search, " ".join(description)]

    for i_page in range(input_pdf.getNumPages()):
        output.addPage(input_pdf.getPage(i_page))
    for parent_h1 in bookmarks_parent:
        par = output.addBookmark(parent_h1.upper(), int(0), parent=None)
        for search, description in bookmarks.items():
            if parent_h1 is search[0]:
                output.addBookmark(str(description[1]), int(search[1]), parent=par)
    all_bookmark = output.addBookmark("Хронологія", int(0), parent=None)
    for search, description in bookmarks.items():
        output.addBookmark(str('\n'.join(description)), int(search[1]), parent=all_bookmark)
    output_file_name = easygui.filesavebox(msg="None", title="title", default=str(output_pdf), filetypes="pdf")
    output_stream = open(output_file_name, 'wb')
    output.write(output_stream)
    output_stream.close()


pdf_to_img()
save_text_to_json()
add_bookmarks()
